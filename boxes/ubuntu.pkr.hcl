
locals {
  timestamp           = regex_replace(timestamp(), "[- TZ:]", "")
  # fmt: ubuntu20.04.3-server-minimal_v0.0.1_20211226005251
  source_path_toarray = split("/", var.source_path)
  vm_info             = split("_", element(local.source_path_toarray, length(local.source_path_toarray) - 2))
  vm_name             = element(local.vm_info, 0)
  vm_version          = element(local.vm_info, 1)
  vm_basename_folder  = "${path.cwd}/builds/${local.vm_name}_${local.vm_version}_${local.timestamp}"
  box_basename_folder = "${path.cwd}/vagrant/${local.vm_name}_${local.vm_version}_${local.timestamp}"
  is_server           = length(regexall(".*server.*", local.vm_name)) > 0 ? "--skip-tags=desktop" : ""
}

# https://www.packer.io/docs/builders/virtualbox/iso
source "virtualbox-ovf" "ubuntu-ovf" {
  boot_wait        = "5s"
  headless         = "${var.headless}"
  shutdown_command = "echo '${var.ssh_username}' | sudo -S shutdown -P now"
  output_directory = "${local.vm_basename_folder}"
  source_path      = "${var.source_path}"
  ssh_password     = "${var.ssh_password}"
  ssh_username     = "${var.ssh_username}"
  ssh_wait_timeout = "10000s"
  vm_name          = "${local.vm_name}"
}

# https://www.packer.io/docs/templates/hcl_templates/blocks/build
build {
  sources = [
    "source.virtualbox-ovf.ubuntu-ovf"
  ]

  provisioner "ansible-local" {
    extra_arguments = [
      "${var.verbose}",
      "--extra-vars",
      "ansible_python_interpreter=/usr/bin/python3",
      "${local.is_server}"
    ]
    # galaxy_file   = "${path.cwd}/ansible/requirements.yml"
    playbook_file = "${path.cwd}/ansible/main.yml"
  }

  post-processors {
    post-processor "vagrant" {
      output = "${local.box_basename_folder}/${local.vm_name}.box"
    }

    post-processor "vagrant-cloud" {
      access_token = "${var.cloud_token}"
      box_tag      = "${var.box_organization}/${local.vm_name}"
      no_release   = true
      version      = substr(local.vm_version, 1, -1)
    }
  }
}


