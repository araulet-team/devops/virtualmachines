# vm_name 
variable "vm_basename" {
  type        = string
  description = "Use to build \"vm_name\". This is the name of the OVF...file for the new virtual machine, without the file extension."
}

variable "vm_version" {
  type        = string
  description = "Use to build \"vm_name\". Version of your box"
}

variable "description" {
  type        = string
  description = "Description of what you are trying to build."
}

# builder args https://www.packer.io/docs/builders/virtualbox/iso
variable "hostname" {
  type        = string
  description = "Used during the boot_command for the preseed"
  default     = "vagrant"
}

variable "vm_name" {
  type        = string
  description = "This is the name of the OVF file for the new virtual machine, without the file extension."
  default     = "default_value"
}

variable "iso_url" {
  type        = string
  description = "A URL to the ISO containing the installation image or virtual hard drive (VHD or VHDX) file to clone."
}

variable "iso_checksum" {
  type        = string
  description = "The checksum for the ISO file or virtual hard drive file"
}

variable "headless" {
  type        = bool
  description = "Packer defaults to building VirtualBox virtual machines by launching a GUI that shows the console of the machine being built. When this value is set to true, the machine will start without a console."
  default     = "false"
}

variable "disk_size" {
  type        = string
  description = "The size, in megabytes, of the hard disk to create for the VM"
  default     = "8192"
}


# virtualbox extra tunning https://www.packer.io/docs/builders/virtualbox/iso#vbox-manage-configuration
variable "memory" {
  type        = string
  description = "The amount of memory to use for building the VM in megabytes."
  default     = "1024"
}

variable "cpus" {
  type        = string
  description = "The number of cpus to use for building the VM. Defaults to 1."
  default     = "1"
}

variable "ram" {
  type        = string
  description = "Video memory assigned."
  default     = "128"
}

# Setup as envs variables and used in the bash script to drive some behavior
variable "desktop" {
  type        = string
  description = "Use true if you are building a desktop version."
  default     = "false"
}

variable "update" {
  type        = string
  description = "Perform a dist-upgrade (all packages and kernel) if set to 1."
  default     = "0"
}

# Setup as envs variables
variable "ftp_proxy" {
  type    = string
  default = "${env("ftp_proxy")}"
}

variable "http_proxy" {
  type    = string
  default = "${env("http_proxy")}"
}

variable "https_proxy" {
  type    = string
  default = "${env("https_proxy")}"
}

variable "no_proxy" {
  type    = string
  default = "${env("no_proxy")}"
}

variable "rsync_proxy" {
  type    = string
  default = "${env("rsync_proxy")}"
}

variable "install_vagrant_key" {
  type        = string
  description = "Create a vagrant user. Needed to ssh and future operation (ansible, vagrant)."
  default     = "true"
}

variable "ssh_password" {
  type    = string
  default = "vagrant"
}

variable "ssh_username" {
  type    = string
  default = "vagrant"
}
